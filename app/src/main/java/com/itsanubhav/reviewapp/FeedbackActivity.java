package com.itsanubhav.reviewapp;

import android.content.DialogInterface;
import android.os.Build;
import android.support.design.internal.NavigationMenu;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FeedbackActivity extends AppCompatActivity {

    private DatabaseHandler databaseHandler;
    private List<ReviewPOJO> list = new ArrayList<>();
    private List<ReviewPOJO> newlist = new ArrayList<>();
    private int inputSelection = 0;
    private boolean newlistFlag = false;
    private int count;
    private LinearLayout linearLayout;
    private TextView bad,good,avg,exc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        Toolbar toolbar = findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Feedback");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        databaseHandler = new DatabaseHandler(this);
        list = databaseHandler.getAllReviewss();
        count = databaseHandler.getContactsCount();
        exc = findViewById(R.id.excellentCount);
        good = findViewById(R.id.goodCount);
        avg = findViewById(R.id.avgCount);
        bad = findViewById(R.id.badCount);
        int c;
        c = databaseHandler.getSpeCount("Bad");
        bad.setText("Bad: "+c);
        c = databaseHandler.getSpeCount("Excellent");
        exc.setText("Excellent: "+c);
        c = databaseHandler.getSpeCount("Good");
        good.setText("Good: "+c);
        c = databaseHandler.getSpeCount("Average");
        avg.setText("Average: "+c);
        linearLayout = findViewById(R.id.mainContainer);
        write();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                showSortDialog();
                return true;
            case R.id.action_reset:
                showDeleteDialog();
                return true;
            case android.R.id.home:
                super.onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showSortDialog(){
        final CharSequence[] items = {"Default","Date",
                "Time",
                "Feedback",
                "Reference"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackActivity.this);
        builder.setTitle("Sort By")
                .setSingleChoiceItems(items, inputSelection, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        inputSelection = i;
                        doAdjustments(inputSelection);
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    private void doAdjustments(int i){
        list.clear();
        newlist.clear();
        switch (i){
            case 0:
                list = databaseHandler.getAllReviewss();
                newlistFlag = false;
                break;
            case 1:
                list = databaseHandler.getAllReviewss();
                newlistFlag = false;
                break;
            case 2:
                list = databaseHandler.getDateSorted();
                newlistFlag = false;
                break;
            case 3:
                list.clear();
                newlist.clear();
                list = databaseHandler.getSpeList("Excellent");
                newlist.addAll(list);
                list = databaseHandler.getSpeList("Good");
                newlist.addAll(list);
                list = databaseHandler.getSpeList("Average");
                newlist.addAll(list);
                list = databaseHandler.getSpeList("Bad");
                newlist.addAll(list);
                list.clear();
                Collections.reverse(newlist);
                newlistFlag = true;
                break;
            case 4:
                list = databaseHandler.getSortedList();
                //list.sort(Comparator.comparing(ReviewPOJO::getRef).reversed());
                newlistFlag = false;
                break;
        }
        write();
    }

    private void write(){
        linearLayout.removeAllViews();
        if(!newlistFlag) {
            for (int i = list.size() - 1; i >= 0; i--) {
                View child = getLayoutInflater().inflate(R.layout.feedback_row, null);
                TextView dateView = child.findViewById(R.id.rowDateView);
                TextView timeView = child.findViewById(R.id.rowTimeView);
                TextView feedbackView = child.findViewById(R.id.rowFedbackView);
                TextView refView = child.findViewById(R.id.rowRefView);
                dateView.setText(list.get(i).getDate());
                timeView.setText(list.get(i).getTime());
                refView.setText(list.get(i).getRef());
                feedbackView.setText(list.get(i).getFeedback());
                linearLayout.addView(child);
            }
        }
        for(int i=newlist.size()-1;i>=0;i--){
            View child = getLayoutInflater().inflate(R.layout.feedback_row, null);
            TextView dateView = child.findViewById(R.id.rowDateView);
            TextView timeView = child.findViewById(R.id.rowTimeView);
            TextView feedbackView = child.findViewById(R.id.rowFedbackView);
            TextView refView = child.findViewById(R.id.rowRefView);
            dateView.setText(newlist.get(i).getDate());
            timeView.setText(newlist.get(i).getTime());
            refView.setText(newlist.get(i).getRef());
            feedbackView.setText(newlist.get(i).getFeedback());
            linearLayout.addView(child);
        }
        newlist.clear();
    }

    public void showDeleteDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackActivity.this);
        builder.setTitle("Delete entry")
                .setMessage("Are you sure you want to delete "+databaseHandler.getContactsCount()+" feedback(s) ?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        databaseHandler.deleteTable();
                        linearLayout.removeAllViews();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .show();
    }
}
