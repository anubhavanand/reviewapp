package com.itsanubhav.reviewapp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class ReviewActivity extends AppCompatActivity {
    private DatabaseHandler databaseHandler;
    private EditText editText;
    private String ref;
    private int rev;
    TextView textView;
    private String currentDateTimeString;
    private Button button;
    private RadioGroup radioGroup;
    private RadioButton a,b,c,d;
    private TextView exc,good,ave,bad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        databaseHandler = new DatabaseHandler(this);
        RelativeLayout constraintLayout = findViewById(R.id.root_review);
        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();
        textView = findViewById(R.id.dateTime);
        editText = findViewById(R.id.refrence);
        setDate();
        exc = findViewById(R.id.exc);
        good = findViewById(R.id.good);
        ave = findViewById(R.id.ave);
        bad = findViewById(R.id.bad);
        radioGroup = findViewById(R.id.reviewRadio);
        a = findViewById(R.id.a);
        b = findViewById(R.id.b);
        c = findViewById(R.id.c);
        d = findViewById(R.id.d);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                if(i == R.id.a){
                    exc.setTextColor(Color.parseColor("#006400"));
                    good.setTextColor(Color.parseColor("#FFFFFF"));
                    ave.setTextColor(Color.parseColor("#FFFFFF"));
                    bad.setTextColor(Color.parseColor("#FFFFFF"));
                    rev = 1;
                }else if(i==R.id.b){
                    good.setTextColor(Color.parseColor("#FFFF00"));
                    exc.setTextColor(Color.parseColor("#FFFFFF"));
                    ave.setTextColor(Color.parseColor("#FFFFFF"));
                    bad.setTextColor(Color.parseColor("#FFFFFF"));
                    rev = 2;
                }else if(i==R.id.c){
                    ave.setTextColor(Color.parseColor("#FFA500"));
                    good.setTextColor(Color.parseColor("#FFFFFF"));
                    exc.setTextColor(Color.parseColor("#FFFFFF"));
                    bad.setTextColor(Color.parseColor("#FFFFFF"));
                    rev = 3;
                }else if(i==R.id.d){
                    bad.setTextColor(Color.parseColor("#FF0000"));
                    good.setTextColor(Color.parseColor("#FFFFFF"));
                    ave.setTextColor(Color.parseColor("#FFFFFF"));
                    exc.setTextColor(Color.parseColor("#FFFFFF"));
                    rev = 4;
                }
            }
        });

    }

    private void setDate(){
        currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        textView.setText(currentDateTimeString);
    }

    private void saveData(String ref,int review, String dateTime){
        String feedback = null;
        dateTime = dateTime.replaceAll(" AM","");
        dateTime = dateTime.replaceAll(" PM","");
        String date = dateTime.substring(0,dateTime.lastIndexOf(" "));
        String time = dateTime.substring(dateTime.lastIndexOf(" ")+1,dateTime.length());
        switch (review){
            case 1: feedback = "Excellent";
                    break;
            case 2: feedback = "Good";
                    break;
            case 3: feedback = "Average";
                    break;
            case 4: feedback = "Bad";
                    break;
            default:
                    feedback = "NULL";
        }
        databaseHandler.addQuestion(new ReviewPOJO(ref,date,time,feedback));
    }

    public void clickSubmitReview(View view){
        ref = editText.getText().toString();
        if(!TextUtils.isEmpty(ref)) {
            if(rev!=0){
                saveData(ref, rev, currentDateTimeString);
                //Creating the LayoutInflater instance
                /*LayoutInflater li = getLayoutInflater();
                //Getting the View object as defined in the customtoast.xml file
                View layout = li.inflate(R.layout.thanks_msg,
                        (ViewGroup) findViewById(R.id.thanks_msg));*/
                /*Toast toast = new Toast(this);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.setView(layout);//setting the view of custom toast layout
                toast.show();*/
                Toast.makeText(getApplicationContext(),"Thanks for the feedback",Toast.LENGTH_LONG).show();
                /*editText.setText("");
                setDate();*/
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
            }else
                Toast.makeText(getApplicationContext(),"Select a smiley to survey",Toast.LENGTH_LONG).show();
        }
        else
            Toast.makeText(getApplicationContext(),"Enter a reference to your survey",Toast.LENGTH_LONG).show();
    }
}
