package com.itsanubhav.reviewapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "feedbacks";

    // Contacts table name
    private static final String TABLE_CONTACTS = "reviews";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_REF_ID = "ref";
    private static final String KEY_DATE = "date";
    private static final String KEY_TIME = "time";
    private static final String KEY_FEEDBACK = "feedback";
    private static final String KEY_TIMESTAMP = "timestamp";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        /*String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TITLE + " TEXT,"
                + KEY_PH_NO + " TEXT" + ")";*/
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + " ("+KEY_ID+" INTEGER PRIMARY KEY,"+ KEY_REF_ID +" TEXT,"+ KEY_DATE +" TEXT,"+ KEY_TIME +" TEXT,"+ KEY_FEEDBACK +" TEXT,\n" +
                ""+ KEY_TIMESTAMP +" TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

        // Create tables again
        onCreate(db);
    }

    public void deleteTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS,null,null);
        db.close();
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    void addQuestion(ReviewPOJO obj) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_REF_ID,obj.getRef());
        values.put(KEY_DATE,obj.getDate());
        values.put(KEY_TIME,obj.getTime());
        values.put(KEY_FEEDBACK,obj.getFeedback());
        values.put(KEY_TIMESTAMP," time('now') ");
        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values);
        db.close(); // Closing database connection
    }

    // Getting single contact
    ReviewPOJO getQue(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CONTACTS, new String[] { KEY_ID,
                        KEY_REF_ID}, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        ReviewPOJO question = new ReviewPOJO();
        question.setRef(cursor.getString(1));
        question.setDate(cursor.getString(2));
        question.setTime(cursor.getString(3));
        question.setFeedback(cursor.getString(4));
        // return contact
        return question;
    }

    // Getting All Contacts
    public List<ReviewPOJO> getAllReviewss() {
        List<ReviewPOJO> postList = new ArrayList<ReviewPOJO>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ReviewPOJO question = new ReviewPOJO();
                question.setRef(cursor.getString(1));
                question.setDate(cursor.getString(2));
                question.setTime(cursor.getString(3));
                question.setFeedback(cursor.getString(4));
                // Adding contact to list
                if(cursor.getString(0)!= null) {
                    postList.add(question);
                }
            } while (cursor.moveToNext());
        }

        // return contact list
        db.close();
        return postList;
    }

    public List<ReviewPOJO> getDateSorted() {
        List<ReviewPOJO> postList = new ArrayList<ReviewPOJO>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS +" ORDER BY CAST("+KEY_TIME+" AS INTEGER) DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ReviewPOJO question = new ReviewPOJO();
                question.setRef(cursor.getString(1));
                question.setDate(cursor.getString(2));
                question.setTime(cursor.getString(3));
                question.setFeedback(cursor.getString(4));
                // Adding contact to list
                if(cursor.getString(0)!= null) {
                    postList.add(question);
                }
            } while (cursor.moveToNext());
        }

        // return contact list
        db.close();
        return postList;
    }


    // Updating single contact
    /*public int updateContact(Post post) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, post.getId()); // Contact Name
        values.put(KEY_TITLE, post.getTitle()); // Contact Phone
        values.put(KEY_DATE,post.getDate());
        values.put(KEY_CONTENT,post.getContent());
        values.put(KEY_FEAT_IMG,post.getFeatImg());
        values.put(KEY_COMMENT_COUNT,post.getCommentCount());

        // updating row
        return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(post.getId()) });
    }*/

    // Deleting single contact
    /*public void deleteContact(QuizPOJO post) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(post.getId()) });
        db.close();
    }*/


    // Getting contacts Count
    public int getContactsCount() {
        int count;
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    public int getSpeCount(String s) {
        int count;
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS +" WHERE "+KEY_FEEDBACK+" = '"+s+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    public List<ReviewPOJO> getSpeListExcept(String s){
        List<ReviewPOJO> postList = new ArrayList<ReviewPOJO>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS +" WHERE "+KEY_FEEDBACK+" != '"+s+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ReviewPOJO question = new ReviewPOJO();
                question.setRef(cursor.getString(1));
                question.setDate(cursor.getString(2));
                question.setTime(cursor.getString(3));
                question.setFeedback(cursor.getString(4));
                // Adding contact to list
                if(cursor.getString(0)!= null) {
                    postList.add(question);
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        // return contact list
        db.close();
        return postList;
    }

    public List<ReviewPOJO> getSpeList(String s){
        List<ReviewPOJO> postList = new ArrayList<ReviewPOJO>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS +" WHERE "+KEY_FEEDBACK+" = '"+s+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ReviewPOJO question = new ReviewPOJO();
                question.setRef(cursor.getString(1));
                question.setDate(cursor.getString(2));
                question.setTime(cursor.getString(3));
                question.setFeedback(cursor.getString(4));
                // Adding contact to list
                if(cursor.getString(0)!= null) {
                    postList.add(question);
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        // return contact list
        db.close();
        return postList;
    }

    public List<ReviewPOJO> getSortedList(){
        List<ReviewPOJO> postList = new ArrayList<ReviewPOJO>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS +" ORDER BY "+KEY_REF_ID+" DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ReviewPOJO question = new ReviewPOJO();
                question.setRef(cursor.getString(1));
                question.setDate(cursor.getString(2));
                question.setTime(cursor.getString(3));
                question.setFeedback(cursor.getString(4));
                // Adding contact to list
                if(cursor.getString(0)!= null) {
                    postList.add(question);
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        // return contact list
        db.close();
        return postList;
    }

}
