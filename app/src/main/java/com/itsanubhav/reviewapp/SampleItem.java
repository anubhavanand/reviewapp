package com.itsanubhav.reviewapp;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

/**
 * Created by Anubhav on 04-08-2017.
 */

public class SampleItem extends AbstractItem<SampleItem, SampleItem.ViewHolder> {
    public String refId,date,time,feedback;

    //The unique ID for this type of item
    @Override
    public int getType() {
        return R.id.root_feedback_row;
    }

    //The layout to be used for this type of item
    @Override
    public int getLayoutRes() {
        return R.layout.feedback_row;
    }

    //The logic to bind your data to the view
    @Override
    public void bindView(ViewHolder viewHolder, List<Object> payloads) {
        //call super so the selection is already handled for you
        super.bindView(viewHolder, payloads);

        //bind our data
        //set the text for the name
        viewHolder.feedbackView.setText(feedback);
        viewHolder.refView.setText(refId);
        viewHolder.timeView.setText(time);
        viewHolder.dateView.setText(date);
    }

    //reset the view here (this is an optional method, but recommended)
    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);
        holder.dateView.setText(null);
        holder.timeView.setText(null);
        holder.refView.setText(null);
        holder.feedbackView.setText(null);
    }

    //Init the viewHolder for this Item
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView refView,feedbackView,dateView,timeView;
        public ViewHolder(View view) {
            super(view);
            this.dateView = view.findViewById(R.id.rowDateView);
            this.feedbackView = view.findViewById(R.id.rowFedbackView);
            this.refView = view.findViewById(R.id.rowRefView);
            this.timeView = view.findViewById(R.id.rowTimeView);
        }
    }
}
