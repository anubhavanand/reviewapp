package com.itsanubhav.reviewapp;

/**
 * Created by Anubhav on 04-08-2017.
 */

public class ReviewPOJO {
    String ref,date,time,feedback;

    public ReviewPOJO() {
        //Empty Constructor
    }

    public ReviewPOJO(String ref, String date, String time, String feedback) {
        this.ref = ref;
        this.date = date;
        this.time = time;
        this.feedback = feedback;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
